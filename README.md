<!--
    Montasim's GitHub Profile
    Created on : 18/5/2021
    Author     : Mohammad Montasim -Al- Mamun Shuvo
    Contact    : montasimmamun@gmail.com

    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    
    Thank you if you like this profile README!

    BUT, please DO NOT copy this and create your profile based on it.
    You can use it as a reference, and copy a part of it, but DO NOT copy
    all of this and create your profile based on it.

    It is very common that you forget to change some information and leave
    mine in your profile.

    Only when you know what you are copying should you paste it. So, again,
    please DO NOT copy this and create your profile based on it.

    ---------------------------------------------------------------------

    What's more, you can find other awesome profile READMEs at
    https://github.com/abhisheknaiidu/awesome-github-profile-readme.
    There could be a profile README that fits you better than this one.

    ---------------------------------------------------------------------
    
    Wish you a good-looking profile README!

                                —— montasim (https://github.com/montasim)
-->

<br/>

<!-- hello nice to meet you text start -->
<div align="center">
    <img loading="lazy" width="30px" src="https://github.com/montasim/montasim/blob/main/media/icons/code.png" alt="code png" />
    <img loading="lazy" src="https://readme-typing-svg.demolab.com?font=Poppins&weight=700&size=30&duration=1&pause=1&color=EB008B&center=true&vCenter=true&repeat=false&width=395&height=29&lines=HELLO%2C+NICE+TO+MEET+YOU" alt="hello nice to meet you svg" />
    <img loading="lazy" width="30px" src="https://github.com/montasim/montasim/blob/main/media/icons/layers.png" alt="layers png" />
</div>
<!-- hello nice to meet you text end -->

<br/>


<!-- about repository badges start -->
<div align="center">
  <img loading="lazy" style="margin-right: 6px" alt="wakatime badge"src="https://wakatime.com/badge/user/bb224c90-7cb7-4c45-953e-a9e26c1cb06c.svg"/>
  <img loading="lazy" style="margin-right: 6px" alt="follow badge"src="https://img.shields.io/github/followers/estevam5s?label=Followed+By&labelColor=EB008B&color=00B8B5"/>
  <!-- <a style="margin-right: 6px" href="https://github.com/montasim/montasim/issues/new?template=Guestbook_entry.md&title=Adding+<username>+to+guestbook">
    <img loading="lazy" alt="Guest Book" src="https://img.shields.io/badge/-%20%F0%9F%96%8B%20Write%20into%20my%20guest%20book-red?style=flat-round&color=00B8B5" />
  </a> -->
  <img loading="lazy" style="margin-right: 6px" alt="thanks badge" src="https://img.shields.io/badge/Thanks%20for%20visiting-!-1EAEDB.svg">
</div>
<!-- about repository badges end -->

<br/>

<!-- skills icons start -->
<div align="center">
  <img loading="lazy" height="40" width="40" src="https://cdn.simpleicons.org/typescript/FFFFFF" alt="typescript icon"/>
  <img loading="lazy" height="40" width="40" src="https://cdn.simpleicons.org/javascript/FFFFFF" alt="javascript icon" />
  <img loading="lazy" height="40" width="40" src="https://cdn.simpleicons.org/react/FFFFFF" alt="react icon" />
  <img loading="lazy" height="40" width="40" src="https://cdn.simpleicons.org/nodedotjs/FFFFFF" alt="nodejs icon" />
  <img loading="lazy" height="40" width="40" src="https://cdn.simpleicons.org/express/FFFFFF" alt="express icon" />
  <img loading="lazy" height="40" width="40" src="https://cdn.simpleicons.org/mongodb/FFFFFF" alt="mongodb icon" />
  <img loading="lazy" height="40" width="40" src="https://cdn.simpleicons.org/sass/FFFFFF" alt="sass icon" />
  <img loading="lazy" height="40" width="40" src="https://cdn.simpleicons.org/tailwindcss/FFFFFF" alt="tailwindcss icon" />
  <img loading="lazy" height="40" width="40" src="https://cdn.simpleicons.org/bootstrap/FFFFFF" alt="bootstrap icon" />
  <img loading="lazy" height="40" width="40" src="https://cdn.simpleicons.org/html5/FFFFFF" alt="html5 icon" />
  <img loading="lazy" height="40" width="40" src="https://cdn.simpleicons.org/css3/FFFFFF" alt="css3 icon" />
  <img loading="lazy" height="40" width="40" src="https://cdn.simpleicons.org/markdown/FFFFFF" alt="markdown icon" />
</div>
<!-- skills icons end -->

<br/>

<!-- tools badge start -->
<div align="center">
  <img loading="lazy" style="margin-right: 6px" alt="git badge" src="https://img.shields.io/badge/GIT-E44C30?style=for-the-badge&logo=git&logoColor=white&color=00B8B5">
  <img loading="lazy" style="margin-right: 6px" alt="github badge" src="https://img.shields.io/badge/GitHub-100000?style=for-the-badge&logo=github&logoColor=white&color=00B8B5">
  <img loading="lazy" style="margin-right: 6px" alt="vscode badge" src="https://img.shields.io/badge/VSCode-0078D4?style=for-the-badge&logo=visual%20studio%20code&logoColor=white&color=00B8B5">
  <img loading="lazy" style="margin-right: 15px" alt="figma badge" src="https://img.shields.io/badge/Figma-F24E1E?style=for-the-badge&logo=figma&logoColor=white&color=00B8B5">
  <img loading="lazy" style="margin-right: 6px" alt="canva badge" src="https://img.shields.io/badge/Canva-%2300C4CC.svg?&style=for-the-badge&logo=Canva&logoColor=white&color=00B8B5">
  <img loading="lazy" style="margin-right: 15px" alt="firebase badge" src="https://img.shields.io/badge/firebase-ffca28?style=for-the-badge&logo=firebase&logoColor=white&color=00B8B5">
  <img loading="lazy" style="margin-right: 6px" alt="vercel badge" src="https://img.shields.io/badge/Vercel-000000?style=for-the-badge&logo=vercel&logoColor=white&color=00B8B5">
  <img loading="lazy" style="margin-right: 6px" alt="linux badge" src="https://img.shields.io/badge/Linux-FCC624?style=for-the-badge&logo=linux&logoColor=white&color=00B8B5">
</div>
<!-- tools badge end -->

<br/>
<br/>
<br/>

<img width="55%" align="right" alt="Github" src="https://raw.githubusercontent.com/onimur/.github/master/.resources/git-header.svg" />

<h4>
  <img src="https://emojis.slackmojis.com/emojis/images/1588866973/8934/hellokittydance.gif?1588866973" alt="Hi" width="42" />
  Programmer (Nestjs Developer)
</h4>

| **About Me**                                                                                                                                                                                                                                     |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| My name is Estevam Souza from Brazil (🇧🇷). I'm passionate about open source projects and I've always loved to produce a lot of content during my career. I am an enthusiastic JavaScript developer who loves solving difficult technical issues. |

<p align="center">
	<a href="https://github.com/estevam5s">
		<img align="center" src="https://streak-stats.demolab.com?user=estevam5s&theme=radical&hide_border=true&border_radius=1&date_format=M%20j%5B%2C%20Y%5D&exclude_days=Sun%2CMon%2CTue%2CWed%2CThu%2CFri%2CSat&card_width=756&stroke=C522EB" alt="crazycoderstein's Github Stats" />
	</a>
</p>
<br />

<!-- [![My Skills](https://skillicons.dev/icons?i=nestjs,js,ts,aws,graphql,linux,postgres,reactivex,mongo,express,prisma,docker,apollo,jenkins,pug)](https://skillicons.dev) -->

<div style="display: flex; align-items: flex-start; align: center">
  <tr>
    </td>
    <td align="center" width="35">
      <a href="#macropower-tech">
        <img src="https://techstack-generator.vercel.app/graphql-icon.svg" alt="icon" width="35" height="35" />
      </a>
    </td>
    <td align="center" width="35">
      <a href="#macropower-tech">
        <img src="https://techstack-generator.vercel.app/gatsby-icon.svg" alt="icon" width="35" height="35" />
      </a>
    </td>
    <td align="center" width="35">
      <a href="#macropower-tech">
        <img src="https://techstack-generator.vercel.app/jest-icon.svg" alt="icon" width="35" height="35" />
      </a>
    </td>
    <td align="center" width="35">
      <a href="#macropower-tech">
        <img src="https://techstack-generator.vercel.app/prettier-icon.svg" alt="icon" width="35" height="35" />
      </a>
    </td>
    <td align="center" width="35">
      <a href="#macropower-tech">
        <img src="https://techstack-generator.vercel.app/redux-icon.svg" alt="icon" width="35" height="35" />
      </a>
    </td>
    <td align="center" width="35">
      <a href="#macropower-tech">
        <img src="https://techstack-generator.vercel.app/restapi-icon.svg" alt="icon" width="35" height="35" />
      </a>
    </td>
    <td align="center" width="35">
      <a href="#macropower-tech">
        <img src="https://techstack-generator.vercel.app/nginx-icon.svg" alt="icon" width="35" height="35" />
      </a>
    </td>
    <td align="center" width="35">
      <a href="#macropower-tech">
        <img src="https://techstack-generator.vercel.app/react-icon.svg" alt="icon" width="35" height="35" />
      </a>
    </td>
    <td align="center" width="35">
      <a href="#macropower-tech">
        <img src="https://techstack-generator.vercel.app/java-icon.svg" alt="icon" width="35" height="35" />
      </a>
    </td>
    <td align="center" width="35">
      <a href="#macropower-tech">
        <img src="https://techstack-generator.vercel.app/python-icon.svg" alt="icon" width="35" height="35" />
      </a>
    </td>
    <td align="center" width="35">
        <img src="https://techstack-generator.vercel.app/github-icon.svg" alt="icon" width="35" height="35" />
    </td>
    <td align="center" width="35">
      <a href="#macropower-tech">
        <img src="https://techstack-generator.vercel.app/docker-icon.svg" alt="icon" width="35" height="35" />
      </a>
    </td>
    <td align="center" width="35">
        <img src="https://techstack-generator.vercel.app/js-icon.svg" alt="icon" width="35" height="35" />
    </td>
    <td align="center" width="35">
        <img src="https://techstack-generator.vercel.app/cpp-icon.svg" alt="icon" width="35" height="35" />
    </td>
    <td align="center" width="35">
        <img src="https://techstack-generator.vercel.app/webpack-icon.svg" alt="icon" width="35" height="35" />
    </td>
    <td align="center" width="35">
        <img src="https://techstack-generator.vercel.app/mysql-icon.svg" alt="icon" width="35" height="35" />
    </td>
    <td align="center" width="35">
        <img src="https://techstack-generator.vercel.app/ts-icon.svg" alt="icon" width="35" height="35" />
    </td>
    <td align="center" width="35">
        <img src="https://techstack-generator.vercel.app/aws-icon.svg" alt="icon" width="35" height="35" />
    </td>
    <td align="center" width="35">
    	<img src="https://github.com/Anmol-Baranwal/Cool-GIFs-For-GitHub/assets/74038190/de038172-e903-4951-926c-755878deb0b4" width="35">
    </td>
    <td align="center" width="35">
	<img src="https://github.com/Anmol-Baranwal/Cool-GIFs-For-GitHub/assets/74038190/398b19b1-9aae-4c1f-8bc0-d172a2c08d68" width="35">
    </td>
  </tr>
  <tr>
  </tr>
</div>

</br>

<div>
    <a href="https://app.daily.dev/Estevamsl"><img src="https://api.daily.dev/devcards/4cb0fd19db754d52bd9de131e34e9ab0.png?r=jzp"  height="250px" align="right"  alt="Estevam Souza Laureth's Dev Card" alt="centered image"/></a>
</div>

</br>

<strong>🏆 Dooboo lab's github stats</strong>

![estevam5s github-stats](https://stats.dooboo.io/api/github-stats-advanced?login=estevam5s)

## My Favorite Linux Distro :

<p align="center">
<a href="https://exodia-os.github.io/exodia-website/">
  <img width="150" hight="150" src="/Images/Predator.png" />
</a>
<p/>

| | | | | |
| --| :--| :--| :--| :--|
| [![Exodia](https://img.shields.io/badge/OS-Exodia%20OS%20Predator-05122A?style=plastic&logo=ArchLinux&color=informational&logoColor=deeppink)](https://exodia-os.github.io/exodia-website/) </br> [![Arch Based](https://img.shields.io/badge/OS-ArchBased-05122A?style=plastic&logo=ArchLinux&color=informational&style=for-the-badge)](https://archlinux.org/) </br> [![ArchCraft](https://img.shields.io/badge/OS-ArchCraft-05122A?style=plastic&logo=ArchLinux&color=informational&logoColor=green)](https://archcraft.io/) </br> [![BlackArchLinux](https://img.shields.io/badge/OS-blackarchLinux-05122A?style=plastic&logo=ArchLinux&logoColor=black&color=informational)](https://blackarch.org/)</br> [![Garuda Linux](https://img.shields.io/badge/OS-GarudaLinux-05122A?style=plastic&logo=ArchLinux&color=informational&logoColor=important)](https://garudalinux.org/) | [![KaliLinux](https://img.shields.io/badge/OS-KaliLinux-05122A?style=plastic&logo=KaliLinux&color=informational)](https://www.kali.org/) </br> [![tails](https://img.shields.io/badge/OS-Tails%20OS-05122A?style=plastic&logo=tails&color=informational)](https://tails.boum.org/) </br> [![Debian Based](https://img.shields.io/badge/OS-DebianBased-05122A?style=plastic&logo=debian&color=informational&style=for-the-badge)](https://www.debian.org/) </br> [![Linuxmint](https://img.shields.io/badge/OS-Linuxmint-05122A?style=plastic&logo=Linuxmint&color=informational)](https://linuxmint.com/) </br> [![ParrotOS](https://img.shields.io/badge/OS-ParrotOS-05122A?style=plastic&logo=debian&color=informational)](https://parrotsec.org/) | [![RedHat](https://img.shields.io/badge/OS-Red%20Hat-05122A?style=plastic&logo=RedHat&color=informational&logoColor=red)](https://www.redhat.com/en) </br> [![In use](https://img.shields.io/badge/OS-In%20use-05122A?style=plastic&logo=linux&color=informational&style=for-the-badge)](https://www.linux.org/) </br> [![Slackware Based](https://img.shields.io/badge/OS-SlackwareBased-05122A?style=plastic&logo=Slackware&color=informational&style=for-the-badge)](http://www.slackware.com/) </br>  [![Fedora Based](https://img.shields.io/badge/OS-FedoraBased-05122A?style=plastic&logo=Fedora&color=informational&style=for-the-badge)](https://getfedora.org/) </br> [![qubes os](https://img.shields.io/badge/OS-QubesOS-05122A?style=plastic&logo=QubesOS&color=informational)](https://www.qubes-os.org/) | [![ArchCraft](https://img.shields.io/badge/OS-ArchCraft-05122A?style=plastic&logo=ArchLinux&color=informational&logoColor=green)](https://archcraft.io/) </br>  [![Exodia](https://img.shields.io/badge/OS-Exodia%20OS%20Predator-05122A?style=plastic&logo=ArchLinux&color=informational&logoColor=deeppink)](https://exodia-os.github.io/exodia-website/) </br>  [![Exodia](https://img.shields.io/badge/OS-Garuda%20OS%20Linux-05122A?style=plastic&logo=ArchLinux&color=informational&logoColor=deeppink)](https://exodia-os.github.io/exodia-website/) </br>  [![Wifi-Slax](https://img.shields.io/badge/OS-WifiSlax-05122A?style=plastic&logo=slackware&color=informational)](https://www.wifislax.com/)   </br>  [![Wifi-Slax](https://img.shields.io/badge/OS-WifiSlax-05122A?style=plastic&logo=slackware&color=informational)](https://www.wifislax.com/)                               | [![KaliLinux](https://img.shields.io/badge/OS-KaliLinux-05122A?style=plastic&logo=KaliLinux&color=informational)](https://www.kali.org/) </br> [![tails](https://img.shields.io/badge/OS-Tails%20OS-05122A?style=plastic&logo=tails&color=informational)](https://tails.boum.org/) </br> [![ParrotOS](https://img.shields.io/badge/OS-ParrotOS-05122A?style=plastic&logo=debian&color=informational)](https://parrotsec.org/) </br> [![Wifi-Slax](https://img.shields.io/badge/OS-WifiSlax-05122A?style=plastic&logo=slackware&color=informational)](https://www.wifislax.com/)  </br> [![Wifi-Slax](https://img.shields.io/badge/OS-WifiSlax-05122A?style=plastic&logo=slackware&color=informational)](https://www.wifislax.com/) |

![](https://user-images.githubusercontent.com/73097560/115834477-dbab4500-a447-11eb-908a-139a6edaec5c.gif)

## :sparkling_heart: Support the project <img src="https://github.githubassets.com/images/mona-whisper.gif" />

[![Linkedin](https://img.shields.io/badge/Linkedin-pink)](https://www.linkedin.com/in/estevam-souza/) °
[![Linkedin](https://img.shields.io/badge/Bitbucket-green)](https://bitbucket.org/Estevamsl/workspace/overview/) °
[![Linkedin](https://img.shields.io/badge/Gitlab-white)](https://gitlab.com/estevam5s) °
[![@3urobeat](https://img.shields.io/badge/Portfolio-lightblue)](https://estevamsouza.com) °
[![My Setup](https://img.shields.io/badge/My%20Setup-lightgreen)](https://gist.github.com/estevam5s/958517e0d8d270c96c20bddd91bf1908) °
[![My Projects](https://img.shields.io/badge/%20My%20Blogger%20-lightblue)](https://bloggertecnology.vercel.app/) °
[![Donate](https://img.shields.io/badge/Donate-pink)](https://github.com/sponsors/estevam5s) °
![visitors](https://vbr.wocr.tk/badge?page_id=Raymo111.Raymo111&color=00cf00) °
<img alt="GitHub followers" src="https://img.shields.io/github/followers/estevam5s?color=green&logo=github">

#### Perfil em Português: <kbd>[<img title="Portugues" alt="Portugues" src="https://img.icons8.com/color/48/000000/brazil.png" width="22">](https://github.com/estevam5s/estevam5s/blob/main/README-us.md)</kbd>

I open-source almost everything I can, and I try to reply to everyone needing help using these projects. Obviously,
this takes time. You can use this service for free.

However, if you are using this project and are happy with it or just want to encourage me to continue creating stuff, there are a few ways you can do it:-

- Giving proper credit when you use github-readme-stats on your readme, linking back to it :D
- Starring and sharing the project :rocket: I'll probably buy a ~~coffee~~ tea. :tea:

<a href="https://github.com/sponsors/estevam5s" target="_blank"><img height="40" alt="GitHub Sponsor" src = "https://img.shields.io/badge/Sponsor me on GitHub-30363D?style=for-the-badge&logo=GitHub-Sponsors&logoColor=#white"></a>

if you like what i do, maybe consider buying me a coffee/tea 🥺👉👈

<a href="https://www.buymeacoffee.com/estevamsl" target="_blank"><img src="https://cdn.buymeacoffee.com/buttons/v2/default-blue.png" alt="Buy Me A Coffee" style="height: 40px !important;width: 145px !important;" ></a> <img align="right" width=100px height=100px alt="side_sticker" src="https://media.giphy.com/media/TEnXkcsHrP4YedChhA/giphy.gif"/>

<p align="center">
  <img src="https://github.com/estevam5s/estevam5s/blob/main/animated/github-contribution-grid-snake-dark.svg" alt="snake"></center>
</p>

<p align="center">
  <a href="https://exodia-os.github.io/exodia-website/"><img src="https://github.com/Exodia-OS/.github/blob/e1ab4df119651217467731aef92f97226d5c193a/profile/GIFs/logos.gif" height="200" width="200" alt="Exodia"></a>
</p>

![](https://user-images.githubusercontent.com/73097560/115834477-dbab4500-a447-11eb-908a-139a6edaec5c.gif)

<p align="center">
	
<img  width="800px" src="https://github.com/HyunCafe/HyunCafe/raw/main/assests/loficity.gif" />

<!-- <img src="gif/00xWolf-HACK-THE-PLANET.gif" alt="00xWolf"> -->
